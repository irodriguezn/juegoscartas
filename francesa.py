from random import randint

"""barajaMazo(): reparteCartas(baraja, num): desglosaCarta(carta): creaCarta(desglose): invierteCarta(desglose): desglosaInvertida(carta): ordenaPalo(mano):
traduceCarta(carta): puntosCarta(carta): puntosMazo(cartas):"""

nombrePalos={'T': 'treboles', 'D': 'diamantes', 'C': 'corazones', 'P': 'picas'}
valorPalos={'4': 'P', '3': 'C', '2': 'D', '1': 'T'}

def muestraCarta(carta, c=0):
    global nombrePalos
    palo=paloCarta(carta)
    valor=valorCarta(carta)
    if c!=0: # mostramos versión larga
        palo=" de "+nombrePalos[palo]
    print( valor + palo + " ", end="")
    
def paloCarta(carta):
    global valorPalos
    paloNum=str(carta[0])
    return valorPalos[paloNum]

def valorCarta(carta):
    valorNum=str(carta[1])    
    if valorNum=='1':
        valor="A"
    elif valorNum=='11':
        valor="J"
    elif valorNum=='12':
        valor="Q"
    elif valorNum=='13':
        valor="K"
    else:
        valor=valorNum
    return valor

def creaBaraja():
    baraja=[]
    carta=[]
    for i in range(1,5):
        for j in range(1,14):
            carta=[i,j]
            baraja.append(carta)
    return baraja

def muestraMazo(mazo, c=0):
    for carta in mazo:
        muestraCarta(carta, c)

def extraeMazo(n, baraja):
    mazo=[]
    for i in range(n):
        mazo.append(baraja.pop())
    return mazo

def barajaMazo(mazo):
    mazoNuevo=[]
    for i in range(len(mazo)):
        pos=randint(0,len(mazo)-1)
        mazoNuevo.append(mazo.pop(pos))
    return mazoNuevo

def ordenaMazo(mazo):
    return sorted(mazo)

def puntosHonor(mazo):
    ph=0
    for carta in mazo:
        valor=valorCarta(carta)
        if valor=='A':
            ph+=4
        elif valor=='K':
            ph+=3
        elif valor=='Q':
            ph+=2
        elif valor=='J':
            ph+=1
    return ph

def getDistribucion(mazo):
    picas=corazones=diamantes=treboles=0
    for carta in mazo:
        palo=paloCarta(carta)
        if palo=="P":
            picas+=1
        elif palo=="C":
            corazones+=1
        elif palo=="D":
            diamantes+=1
        else:
            treboles+=1
    return picas, corazones, diamantes, treboles

def puntosDistribucion(distribucion):
    pd=0
    for num in distribucion:
        if num==0:
            pd+=3
        elif num==1:
            pd+=2
        elif num==2:
            pd+=1
    return pd


baraja=creaBaraja()
baraja=barajaMazo(baraja)
norte=extraeMazo(13,baraja)
norte=ordenaMazo(norte)
ph=puntosHonor(norte)
dis=getDistribucion(norte)
pd=puntosDistribucion(dis)
print("NORTE")
print("-----")
muestraMazo(norte)
print()
print(dis)
print("PH:",ph,"PD:",pd, "PT:",ph+pd)
print()
sur=extraeMazo(13,baraja)
sur=ordenaMazo(sur)
ph=puntosHonor(sur)
dis=getDistribucion(sur)
pd=puntosDistribucion(dis)
print("SUR")
print("-----")
muestraMazo(sur)
print()
print(dis)
print("PH:",ph,"PD:",pd, "PT:",ph+pd)
