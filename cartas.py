from random import randint

def barajaMazo():
    lista1=[]
    for i in ["O","E","C","B"]:
        for j in range(1,13):
            lista1.append(str(j)+i)
    lista2=[]
    for i in range(48):
        pos=randint(0,len(lista1)-1)
        lista2.append(lista1.pop(pos))
    return lista2

def reparteCartas(baraja, num):
    mano=[]
    for i in range(num):
        pos=randint(0,len(baraja)-1)
        mano.append(baraja.pop(pos))
    return mano

def desglosaCarta(carta):
    palo=carta[-1]
    valor=carta[0:len(carta)-1]
    c={}
    c["palo"]=palo
    c["valor"]=valor
    return c

def creaCarta(desglose):
    palo=desglose["palo"]
    valor=desglose["valor"]
    carta=valor+palo
    return carta

def invierteCarta(desglose):
    palo=desglose["palo"]
    valor=desglose["valor"]
    carta=palo+valor
    return carta

def desglosaInvertida(carta):
    palo=carta[0]
    valor=carta[1:]
    c={}
    c["palo"]=palo
    c["valor"]=valor
    return c

def ordenaPalo(mano):
    lista=[]
    lista2=[]
    for carta in mano:
        c=desglosaCarta(carta)
        cartaInv=invierteCarta(c)
        lista.append(cartaInv)
    lista.sort()
    for carta in lista:
        c=desglosaInvertida(carta)
        lista2.append(creaCarta(c))
    return lista2

def traduceCarta(carta):
    c=desglosaCarta(carta)
    cartaLetra=c["valor"]+" de "
    paloLetra=""
    palo=c["palo"]
    if palo=="O":
        paloLetra="oros"
    elif palo=="E":
        paloLetra="espadas"
    elif palo=="C":
        paloLetra="copas"
    elif palo=="B":
        paloLetra="bastos"
    cartaLetra+=paloLetra
    return cartaLetra

def puntosCarta(carta):
    puntos=0
    c=desglosaCarta(carta)
    valor=c["valor"]
    if valor=="1":
        puntos=11
    elif valor=="3":
        puntos=10
    elif valor=="12":
        puntos=4
    elif valor=="11":
        puntos=3
    elif valor=="10":
        puntos=2
    return puntos

def puntosMazo(cartas):
    puntos=0
    for carta in cartas:
        puntos+=puntosCarta(carta)
    return puntos

baraja=barajaMazo()
mano=reparteCartas(baraja,15)
print(mano)
print(puntosMazo(mano))
print(ordenaPalo(mano))
